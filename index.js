// Bai 1: Tinh lương nhân viên

/**
 * Đầu vào
 *
 *  Lương 1 ngày: 100.000
 *  Số ngày làm: 06 ngày
 *
 * Các bước xử lý
 *  B1: Tạo biến cho tiền lương mỗi ngày: dailySalary
 *  B2: Tạo biến cho số ngày đi làm: day
 *  B3: Tạo biến và tính tỏng tiền lương: result = dailySalary * day
 *  B4: In ra kết quả console
 *
 * Đầu ra
 *  Tổng tiền lương NV: 100.000 * 6 = 600.000
 */

var dailySalary = 100000,
  day = 06;
var result = dailySalary * day;
console.log("result = ", result);


// Bài 2:
/**
 *Đầu vào:
 *  5 số thực: 1, -2, 3, -5, 7
 *
 * Các bước xử lý:
 * Tạo biến và khai báo số thực thứ 1: number1 = 1
 * Tạo biến và khai báo số thực thứ 2: number2 = -2
 * Tạo biến và khai báo số thực thứ 3: number3 = 3
 * Tạo biến và khai báo số thực thứ 4: number4 = -5
 * Tạo biến và khai báo số thực thứ 5: number5 = 7
 * Tạo biến lưu kết quả trung bình tổng 5 số thực vừa khai báo: average
 * average = (number1 + number2 + number3 + number4 + number5) / 5
 * In ra kết quả console
 *
 * Đầu ra:
 * Giá trị trung bình tổng 5 số: 0.8
 */

 var number1, number2, number3, number4, number5;
 number1 = 1;
 number2 = -2;
 number3 = 3;
 number4 = -5;
 number5 = 7;
 var average = (number1 + number2 + number3 + number4 + number5) / 5;
 console.log('average: ', average);
// Bài 3

/**
 * Đầu vào:
 *  tí giá = 23500
 *  số lượng dollar muốn đổi sang VNS: 5$
 *
 * Các bước xử lý:
 * Tạo biến khai báo tỉ giá: rate
 *  rate = 23500
 *  Tạo biến và khai báo số usd muốn đổi: dollarWantToChange = 5$
 * Tạo biến result để lưu kết quả sau khi xuất ra VND
 * result = rate * dollarWantToChange
 * In ra kết quả console
 *
 * Đầu ra:
 * Số tiền sau khi đổi $5 sang VND:  Result = 117.500 VND
 *
 */
var rate = 23500;
var dollarWantToChange = 5;
var result = rate * dollarWantToChange;
console.log("result: ", result, "VND");

// Bài 4:
/**
 * Đầu vào:
 *  chiều dài:  length = 4
 *  chiều rộng: width = 3
 *
 *
 * Các bước xử lý:
 *  Tạo biến và khai báo chiều dài: length = 4
 *  Tạo biến và khai báo chiều rộng: width = 3
 *  Tạo biến lưu kết quả diện tích area
 *  Tạo biến lưu kết quả chu vi perimeter
 *  Cách tính: area = length * width
 *             perimeter = (lenft + width ) * 2
 *  In ra kết quả console
 * Đầu ra:
 * Chu vi: 14
 * Diện tích: 12
 */

var length = 4;
var width = 3;
var area;
var perimeter;
area = length * width;
perimeter = (length + width) * 2;
console.log({area, perimeter});


//Bài tập 5:Tính tổng 2 kí số

/**
 * Đầu vào:
 *
 *  Số tự nhiên nhập vào: 68
 *
 * Các bước xử lý:
 *
 *  Tạo biến cho số tự nhiên nhập vào: number
 *  Tạo biến cho hàng đơn vị: units
 *  Tạo biến cho hàng chục: dozen
 *  Tính số hàng chục: lấy number chia cho 10, lấy kết quả làm tròn ( dùng Math.floor)
 *  Tính số hàng đơn vị: lấy number chia lấy dư cho 10
 *  Tạo biến result để lưu kết quả tổng 2 kí số: result = units + dozens
 *  In ra kết quả console
 *
 * Đầu ra:
 *  Tổng kí số result = 14
 */
 var number = 68;
 var units;
 units = Math.floor(number / 10);
 var dozens;
 dozens = number % 10;
 var result = units + dozens;
 console.log("result: ", result);